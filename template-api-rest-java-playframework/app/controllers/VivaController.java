package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.Imovel;
import models.ImovelService;
import play.data.Form;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class VivaController extends Controller {

    static Form<Imovel> imovelForm = Form.form(Imovel.class);

    public Result jsonResult(Result httpResponse) {
        response().setContentType("application/json; charset=utf-8");
        return httpResponse;
    }



    @Transactional(readOnly = true)
    public Result rectangle(Integer ax, Integer ay, Integer bx, Integer by) {

        List<Imovel> imovel = ImovelService.findRectangule(ax, ay, bx, by);

        return jsonResult(ok(Json.toJson(imovel)));
    }

    @Transactional(readOnly = true)
    public Result get(Integer id) {
        Imovel imovel = ImovelService.find(id);
        if (imovel == null) {
            ObjectNode result = Json.newObject();
            result.put("error", "Not found " + id);
            return jsonResult(notFound(result));
        }
        return jsonResult(ok(Json.toJson(imovel)));
    }


    @Transactional
    public Result create() {
        Form<Imovel> imovel = imovelForm.bindFromRequest();

        if (imovel.hasErrors()) {
            return jsonResult(badRequest(imovel.errorsAsJson()));
        }

        ObjectNode imovelValido = valida(imovel);
        if(imovelValido.get("error")!=null){
            System.out.println(imovelValido.get("error"));
            return jsonResult(badRequest(String.valueOf(imovelValido.get("error"))));
        }
        System.out.println(imovel);
        imovel.data().put("provinces", (imovelValido.get("provinces").toString()));
        fromJson(imovel.data());
        Imovel newImovel = ImovelService.create(fromJson(imovel.data()));


        return jsonResult(created(Json.toJson(newImovel)));
    }

    private Imovel  fromJson(Map<String, String> data) {
        Imovel imovel = new Imovel();
        imovel.x = Integer.valueOf(data.get("x"));
        imovel.y = Integer.valueOf(data.get("y"));
        imovel.beds = Integer.valueOf(data.get("beds"));
        imovel.baths = Integer.valueOf(data.get("baths"));
        imovel.squareMeters = Integer.valueOf(data.get("squareMeters"));
        imovel.provinces = data.get("provinces");
        return imovel;
    }

    /**
     * Update an employee with the data of request
     *
     * @return Result
     */
    @Transactional
    public Result update() {
        Form<Imovel> imovel = imovelForm.bindFromRequest();
        if (imovel.hasErrors()) {
            return jsonResult(badRequest(imovel.errorsAsJson()));
        }
        Imovel updatedImovel = ImovelService.update(imovel.get());
        return jsonResult(ok(Json.toJson(updatedImovel)));
    }

    /**
     * Delete an employee by id
     */
    @Transactional
    public Result delete(Integer id) {
        if (ImovelService.delete(id)) {
            ObjectNode result = Json.newObject();
            result.put("msg", "Deleted " + id);
            return jsonResult(ok(result));
        }
        ObjectNode result = Json.newObject();
        result.put("error", "Not found " + id);
        return jsonResult(notFound(result));
    }


    public ObjectNode valida(Form<Imovel> imovel){
        Integer x = Integer.valueOf(imovel.data().get("x"));
        Integer y = Integer.valueOf(imovel.data().get("y"));
        Integer  beds= Integer.valueOf(imovel.data().get("beds"));
        Integer baths = Integer.valueOf(imovel.data().get("baths"));
        Integer squareMeters = Integer.valueOf(imovel.data().get("squareMeters"));

        ObjectNode result = Json.newObject();

        if (!(x<=0 && x<=1400) &&!(y>=0 && y<= 1000)) {
            result.put("error", "Imóvel está fora da area de Spotippos ");
        }
        if (!(baths>=1 && baths <=4)){
            result.put("error", "Número de banheiros inválido");

        }
        if(!(squareMeters>=20 && squareMeters <=240)){
            result.put("error", "Area da casa inválida");
        }
        if(!(beds >=1 && beds <=5)){
            result.put("error", "Numero de quartos inválido");
        }

        result = verifyProvince(x,y);

        return result;
    }


    private ObjectNode verifyProvince(Integer x, Integer y) {
        ObjectNode result = Json.newObject();

        if((x >= 0 && x <= 400) && (y>=0 && y<=500)) {
            result.put("provinces", "Gode");
        }
        if((x>=401 && x<=500) && (y>=0 && y<=500)){
            List<String> list = new ArrayList<>();
            list.add("Gode");
            list.add("Ruja");
            result.put("provinces", String.valueOf(list));
        }
        if ((x>=501 && x<=1000) && (y>=0 && y<=500)){
            result.put("provinces", "Ruja");
        }
        if ((x>=1001 && x<=1400) && (y>=0 && y<=500)){
            result.put("provinces", "Jaby");
        }

        if ((x>=0 && x<=500) && (y>=501 && y<=1000)){
            result.put("provinces", "Scavy");
        }
        if ((x>=501 && x<=900) && (y>=501 && y<=1000)){
            result.put("provinces", "Groola");
        }
        if ((x>=901 && x<=1400) && (y>=501 && y<=1000)){
            result.put("provinces", "Nova");
        }
    return result;
    }


}
