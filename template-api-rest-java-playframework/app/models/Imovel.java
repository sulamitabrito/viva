package models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Imovel {
    public static String TABLE = Imovel.class.getSimpleName();

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer id;

    public Integer x;
    public Integer y;
    public Integer beds;
    public Integer baths;
    public Integer squareMeters;
    public String provinces;

    public Imovel() {
    }

    public Imovel(Integer id, Integer x, Integer y, Integer beds, Integer baths, Integer squareMeters) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.beds = beds;
        this.baths = baths;
        this.squareMeters = squareMeters;
    }

    @Override
    public String toString() {
        return "Imovel{" +
                "id=" + id +
                ", x=" + x +
                ", y=" + y +
                ", beds=" + beds +
                ", baths=" + baths +
                ", squareMeters=" + squareMeters +
                ",provinces="+ provinces+
                '}';
    }
}