package models;

import play.db.jpa.JPA;

import java.util.List;

public class ImovelDAO {
    /**
     * Create an Imovel
     */
    public static Imovel create(Imovel model) {
        JPA.em().persist(model);
        // Flush and refresh for check
        JPA.em().flush();
        JPA.em().refresh(model);
        return model;
    }

    /**
     * Find an imovel by id
     */
    public static Imovel find(Integer id) {
        return JPA.em().find(Imovel.class, id);
    }

    /**
     * Update an imovel
     **/
    public static Imovel update(Imovel model) {
        return JPA.em().merge(model);
    }

    /**
     * Delete an imovel by id
     */
    public static void delete(Integer id) {
        Imovel model = JPA.em().getReference(Imovel.class, id);
        JPA.em().remove(model);
    }

    /**
     * Get all imoveis
     *
     * @return List<Imovel>
     */
    public static List<Imovel> all() {
        return (List<Imovel>) JPA.em().createQuery("SELECT m FROM " + Imovel.TABLE + " m ORDER BY id").getResultList();
    }

    public static List<Imovel> paginate(Integer page, Integer size) {
        return (List<Imovel>) JPA.em().createQuery("FROM " + Imovel.TABLE + " m ORDER BY id").setFirstResult(page * size).setMaxResults(size).getResultList();
    }

    public static List<Imovel> findRectangule(Integer ax, Integer ay, Integer bx, Integer by) {
        return (List<Imovel>) JPA.em().createQuery("FROM " + Imovel.TABLE + " WHERE (x >= :ax and x <= :ay )and (y >= :bx and y <= :by)").setParameter("ax", ax).setParameter("ay", ay).setParameter("bx", bx).setParameter("by", by).getResultList();

    }

}