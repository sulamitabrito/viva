package models;

import play.db.jpa.JPA;

import java.util.List;

public class ImovelService {
    /**
     * Create an Imovel
     */
    public static Imovel create(Imovel data) {
        return ImovelDAO.create(data);
    }

    /**
     * Update an Imovel
     */
    public static Imovel update(Imovel data) {
        return ImovelDAO.update(data);
    }

    /**
     * Find an Imovel by id
     */
    public static Imovel find(Integer id) {
        return ImovelDAO.find(id);
    }

    /**
     * Delete an Imovel by id
     */
    public static Boolean delete(Integer id) {
        Imovel imovel = ImovelDAO.find(id);
        if (imovel != null) {
            ImovelDAO.delete(id);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get all Imoveis
     */
    public static List<Imovel> all() {
        return ImovelDAO.all();
    }

    /**
     * Get the page of employees
     */
    public static List<Imovel> findRectangule(Integer ax, Integer ay, Integer bx, Integer by) {
        return ImovelDAO.findRectangule(ax, ay, bx, by);
    }


}